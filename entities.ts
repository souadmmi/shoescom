export interface Categorie {
    id?: number ,
    produits?: number ,
    name: string 
}

export interface Image{

    id?:number ,
    url:string ,
    produit?:Produit 
}

export interface Commande {
    id?: number ,
    date: string ,
    prix: number ,
    panier?: number ,
    user?: number 
}

export interface LignePanier {
    id?: number ,
    quantity: number ,
    prix: number ,
    taille?: number
    commande?: number 
}

export interface Note {
    id?: number ,
    content: string ,
    date: string ,
    produit?: Produit ,
    author?: User
}

export interface Produit {
    id?: number ,
    name: string ,
    description: string ,
    image: string ,
    prix?: number ,
    marque: string ,
    categorie?: number ,
    images?:Image[],
    taille?: Taille[]

}

export interface Taille {
    id?: number ,
    taille: number ,
    stock: number ,
    panier?: number ,
    produit?: number
}

export interface User {
    id?: number ,
    email: string ,
    name: string ,
    password?: string ,
    role?: string ,
    notes?: number ,
    commandes?: number
}
